//1.
db.fruits.aggregate([

	{$match: {supplier:"Red Farms Inc."}},
	{$count: "itemsRedFarms"}

	])

//2.
db.fruits.aggregate([

{$match:{price:{$gt:50}}},
{$count: "itemsPriceGreatherThan50"}

])


//3.
db.fruits.aggregate([

{$match: {onSale:true}},
{$group: {_id:"$supplier", avgPrice:{$avg:"$price"}}}


])

//4.
db.fruits.aggregate([

{$match: {onSale:true}},
{$group: {_id:"highestPriceOnsale", maxPrice:{$max: "$price"}}}

])

//5.
db.fruits.aggregate([

{$match: {onSale:true}},
{$group: {_id:"minPriceOnSale", minPrice:{$min: "$price"}}}
])