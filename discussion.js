db.fruits.insertMany([
{
    "name": "Apple",
    "supplier": "Red Farms Inc.",
    "stocks": 20,
    "price": 40,
    "onSale": true

},

{
    "name": "Banana",
    "supplier": "Yellow Farms",
    "stocks": 15,
    "price": 20,
    "onSale": true

},

{
    "name": "Kiwi",
    "supplier": "Green Farming and Canning",
    "stocks": 25,
    "price": 50,
    "onSale": true

},

{
    "name": "Mango",
    "supplier": "Yellow Farms",
    "stocks": 10,
    "price": 60,
    "onSale": true

},

{
    "name": "Dragon Fruit",
    "supplier": "Red Farms Inc.",
    "stocks": 10,
    "price": 60,
    "onSale": true

},

])

//Agreation 2-3 steps.

db.fruits.aggregate([
    //The first pipeline is $match. used to pass documents or get documents
    //satisfies or match our condition.
    //syntax: {$match: {field:<value>}}
    //$group - allows us to group together documents and create an
    //analysis out of these grouped documents
    //_id: in $group, we are essentially associating an id to our result.
    //_id: $supplier-we grouped our matched documents based on the
    //value of their supplier field.
    //we crated a new field for this aggregated result called totalStocks.
    //we used $sum to get the sum of all the values of the grouped
    //documents per the indicated field: $stocks
    {$match: {onSale:true}},
    //match stage: apple,mango,kiwi,dragon fruit
    {$group: {_id:"$supplier",totalStocks:{$sum: "$stocks"}}}
    //group stage:
    //supplier: Red Farms -apple,dragon fruit- totalStocks: stocks20+stock:10
    //supplier: Green Framing - kiwi - totalstocks: stocks:25
    //supplier: Yellow Farms - mango - totalstocks: stocks:10




//Mini Activity:
// In a new aggreation for the fruits collection:
//Get the total stocks of items supplied by Red Farms.
db.fruits.aggregate([

    {$match: {supplier:"Red Farms Inc.",onSale:true}},//multiple
    //{$match: {onSale:true}},
    {$group: {_id:"Red Farms Inc.",totalStocks:{$sum: "$stocks"}}}

])

//regex query operators
db.fruits.aggregate([

    {$match: {supplier:{$regex:'yellow',$options:'$i'}}},
    {$group: {_id:"yellowSupplier",totalStocks:{$sum: "$stocks"}}}

])

//avg
db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier", avgStock:{$avg: "$stocks"}}}

])

//avg getting price
db.fruits.aggregate([

{$match: {onSale:true}},
{$group: {_id:"$supplier", avgPrice:{$avg:"$price"}}}


])

//max getting maxStock
db.fruits.aggregate([

{$match: {onSale:true}},
{$group: {_id:"highestStockOnsale", maxStock:{$max: "$stocks"}}}

])

//$supplier
db.fruits.aggregate([

{$match: {onSale:true}},
{$group: {_id:"$supplier", maxStock:{$max: "$stocks"}}}

])


//lowest
db.fruits.aggregate([

{$match: {onSale:true}},
{$group: {_id:"minStockOnSale", minStock:{$min: "$stocks"}}}
])


//Mini Activity:
//get The lowest number of stocks 
//for all items which price is lower than 50.


db.fruits.aggregate([

{$match: {price:{$lt:50}}},
{$group: {_id:"minStockLoss50", minStock:{$min: "$stocks"}}}
])

//$count
db.fruits.aggregate([

{$match: {onSale:true}},
//match-apple,kiwi,mango,banana,dragonFruit
{$count: "itemsOnSale"}
//numberOfDocuments that matched:5

])


//count of all items less than 50
db.fruits.aggregate([

{$match:{price:{$lt:50}}},
{$count: "itemsPriceLess50"}

])

//$out
db.fruits.aggregate([

{$match: {onSale:true}},
{$group: {_id:"$supplier",totalStock:{$sum: "$stocks"}}},
{$out:"stocksSupplier"}
])